import {Component, ElementRef, ViewChild} from '@angular/core';
import {MatTabChangeEvent} from '@angular/material/tabs';
import {FormBuilder, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {NgxCsvParser} from 'ngx-csv-parser';
import {Store} from '@ngxs/store';

import {CsvFormService} from '@app/core/store/csv-form/csv-form.service';
import {WarningComponent} from '@app/popups/warning/warning.component';
import {CsvFormModel} from '@app/core/store/csv-form/csv-form.model';
import {AppState} from '@app/core/store';


/*
CSV File flow.

                     +-------------------+
                     | onCSVFileChange() |
                     +---------+---------+
                               |
                               |
                               |
           +-------------------v--------------------+
           | saveTempCSVData()                      |
           | update CSV and replace "\n" with a ";" |
           | to save rows separation in textarea    |
           +-------------------+--------------------+
                               |
                               |
                               |
          +--------------------v---------------------+
          | parseCSVTextarea()                       |
          | the last source of truth is textarea,    |
          | convert data from it for chart and table |
          +------------------------------------------+

 */

enum AppComponentTabs {
  input,
  output
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChild('uploadInputCSV', {static: false}) uploadInputCSV: ElementRef<HTMLInputElement>;

  AppComponentTabs = AppComponentTabs;
  activeTabIndex = 0;
  form = this.formBuilder.group({
    name: ['', Validators.required],
    gender: ['', Validators.required],
    age: ['', [Validators.required, Validators.min(18), Validators.max(120)]],

    email: ['', [Validators.required, Validators.email]],
    country: [{formatted_address: ''}, Validators.required],
    city: [{formatted_address: ''}, Validators.required],

    uploadedCSVFile: [''],
    uploadedCSVFileName: [''],
    date: ['', Validators.required],

    textCSV: ['', [Validators.required, Validators.pattern(/.+(?=,).+(;)/g)]],
  });

  CSVData: Array<string[]>;

  tempCSVFileEvent: Event;
  tempCSV: { name: string, data: string } = {
    name: null,
    data: null
  };


  /**
   * Parse CSV from textarea format to CSV dataa
   */
  private static parseCSVTextarea(csv: string): Array<string[]> {
    return csv.split(';').map(item => item.split(','));
  }

  constructor(
    public matDialog: MatDialog,
    public matSnackBar: MatSnackBar,
    private store: Store,
    private formBuilder: FormBuilder,
    private ngxCsvParser: NgxCsvParser,
    private csvFormService: CsvFormService,
  ) {
    this.form.valueChanges.subscribe((value: CsvFormModel) => {
      this.csvFormService.saveCSVForm(value);
    });

    this.initFormFromLocalStorage();
  }

  get uploadedCSVFileName() {
    return this.form.get('uploadedCSVFileName').value;
  }


  /**
   * Update form from localstorage on page refresh
   */
  initFormFromLocalStorage(): void {
    const form: CsvFormModel = this.store.selectSnapshot((state: AppState) => state.csvForm.form);

    if (form) {
      this.form.patchValue({
        ...form,
        uploadedCSVFile: null,
        uploadedCSVFileName: null
      });
    }
  }


  /**
   * Form submit event
   */
  onProceed(): void {
    if (this.form.valid) {
      this.activeTabIndex = AppComponentTabs.output;
      this.CSVData = AppComponent.parseCSVTextarea(this.form.get('textCSV').value);
    }
  }


  /**
   * Tabs change listener
   * @param $event - material tab event containing tab index
   */
  onTabChange($event: MatTabChangeEvent): void {
    this.activeTabIndex = $event.index;
  }


  /**
   * Clear uploaded CSV file, since the user has edited the data
   */
  onUserInputCSVChange(): void {
    this.tempCSV.data = null;
    this.tempCSV.name = null;
    this.form.get('uploadedCSVFile').setValue(null);
    this.form.get('uploadedCSVFileName').setValue(null);
  }


  /**
   * Save CSV data
   */
  saveCSVData(): void {
    if (this.form.get('uploadedCSVFile').value) {
      this.form.get('textCSV').setValue(this.tempCSV.data);
      this.resetCSVValidation();
    } else {
      this.onCSVNullUploadSnack();
    }
  }


  /**
   * CSV file listener
   * @param $event - input event containing file
   */
  onCSVFileChange($event: Event): void {
    // get files from the event
    const file = ($event.srcElement as HTMLInputElement).files[0];

    if (!file) {
      // user canceled file selection
      return;
    }

    if (this.form.get('textCSV').value) {
      // user wanna upload a new file
      this.tempCSVFileEvent = $event;
      this.onCSVFileChangeConfirmationDialog();
      return;
    }

    // Parse the file you want to select for the operation along with the configuration
    this.ngxCsvParser
      .parse(file, {header: false, delimiter: ','})
      .pipe().subscribe((result: Array<any>) => {
      this.saveTempCSVData(file, result);
    }, () => {
      this.setCSVErrors();
    });
  }


  /**
   * Save CSV data to temp variable
   * @param file - file with CSV data
   * @param result - CSV data array
   */
  private saveTempCSVData(file: File, result: Array<any>): void {
    // update CSV and replace "\n" with a ";" to save rows separation in textarea
    this.tempCSV.data = result.join(';');
    this.tempCSV.name = file.name;
    this.form.get('uploadedCSVFileName').setValue(this.tempCSV.name);
  }


  /**
   * Trigger CSV input errors
   */
  private setCSVErrors(): void {
    this.form.get('textCSV').setValue(null);
    this.form.get('textCSV').markAsTouched();
    this.form.get('uploadedCSVFile').setValue(null);
    this.form.get('uploadedCSVFileName').setValue(null);
    this.form.get('uploadedCSVFileName').setErrors({notCSVFile: true});
  }


  /**
   * Reset CSV input validation
   */
  private resetCSVValidation(): void {
    this.form.get('uploadedCSVFileName').setErrors(null);
    this.form.get('textCSV').setErrors(null);
  }


  /**
   * Show confirmation dialog on csv file change
   */
  private onCSVFileChangeConfirmationDialog() {
    const dialogRef = this.matDialog.open(WarningComponent);

    dialogRef.afterClosed().subscribe((bool: boolean) => {
      if (bool) {
        this.form.get('textCSV').setValue(null);
        this.onCSVFileChange(this.tempCSVFileEvent);
      }
    });
  }


  /**
   * Show snack if the user has not uploaded file
   */
  private onCSVNullUploadSnack() {
    this.matSnackBar.open('Please upload a CSV file firs', null, {duration: 3000});
  }
}
