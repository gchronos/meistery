import {MatGoogleMapsAutocompleteModule} from '@angular-material-extensions/google-maps-autocomplete';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {HighchartsChartModule} from 'highcharts-angular';
import {TextFieldModule} from '@angular/cdk/text-field';
import {BrowserModule} from '@angular/platform-browser';
import {NgxCsvParserModule} from 'ngx-csv-parser';
import {AgmCoreModule} from '@agm/core';
import {NgModule} from '@angular/core';

import {WarningComponent} from './popups/warning/warning.component';
import {TableComponent} from './components/table/table.component';
import {ChartComponent} from './components/chart/chart.component';
import {MaterialModule} from '@app/core/modules/material.module';
import {StoreModule} from '@app/core/modules/store.module';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';


@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    ChartComponent,
    WarningComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    StoreModule,
    HighchartsChartModule,
    NgxCsvParserModule,
    NgxDatatableModule,
    TextFieldModule,
    MatGoogleMapsAutocompleteModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDd6RB0g1uLgRwXAVApSuMhJB2pGvXck6o',
      libraries: ['places']
    })
  ],
  providers: [
    MatDatepickerModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
