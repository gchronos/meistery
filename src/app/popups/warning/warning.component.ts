import {MatDialogRef} from '@angular/material/dialog';
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-warning',
  templateUrl: './warning.component.html',
  styleUrls: ['./warning.component.scss']
})
export class WarningComponent implements OnInit {

  constructor(
    public matDialogRef: MatDialogRef<WarningComponent>,
  ) {
  }

  ngOnInit(): void {
  }

  onCancel(): void {
    this.matDialogRef.close();
  }

  onConfirm(): void {
    this.matDialogRef.close(true);
  }
}
