import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have two tabs'`, () => {
  });

  it(`should have correct labels of the tabs'`, () => {
  });

  it('should be able to upload file', () => {
  });

  it('should be able submit form', () => {
  });

  it('should chart should have correct data', () => {
  });

  it('should table should have correct data', () => {
  });

  it('should be able to filter table data', () => {
  });
});
