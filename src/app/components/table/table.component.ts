import {ColumnMode, DatatableComponent} from '@swimlane/ngx-datatable';
import {Component, Input, OnInit, ViewChild} from '@angular/core';


interface TableComponentTable {
  columns: { name: string }[];
  rows: Array<{
    [key: string]: string
  }>;
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @ViewChild(DatatableComponent) tableEl: DatatableComponent;

  @Input() set data(data: Array<any>) {
    if (data) {
      this.tempTableRows = [...data];
      this.table = TableComponent.buildTable(data);
    }
  }

  ColumnMode = ColumnMode;
  tempTableRows = [];
  table: TableComponentTable = {
    rows: [],
    columns: []
  };


  /**
   * Build table based on CSV data
   * @param result - CSV data array
   */
  private static buildTable(result: Array<any>): TableComponentTable {
    const [header, ...body] = result;
    return {
      columns: header.map(item => ({name: item})),
      rows: body.map(row => {
        const [product, price] = row;
        return {product, price};
      })
    };
  }


  constructor() {
  }


  ngOnInit(): void {
  }


  /**
   * Table price filter handler
   */
  onTablePriceFilterChange(event) {
    const val = event.target.value.toLowerCase();

    // update the rows
    this.table.rows = this.tempTableRows.filter((d) => {
      return d.price.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // Whenever the filter changes, always go back to the first page
    this.tableEl.offset = 0;
  }
}
