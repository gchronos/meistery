import {Component, Input, OnInit} from '@angular/core';
import {SeriesOptionsType} from 'highcharts';
import * as Highcharts from 'highcharts';


interface BuildBarsChartResponse {
  yAxisTitle: string;
  chartTitle: string;
  series: Array<SeriesOptionsType>;
}

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
  @Input() set data(data: Array<any>) {
    if (data) {
      const charData: BuildBarsChartResponse = ChartComponent.buildBarsChart(data);
      this.chartOptions = {
        ...this.chartOptions,
        yAxis: {title: {text: charData.yAxisTitle}},
        title: {text: charData.chartTitle},
        series: charData.series
      };

      // TODO:
      //  check highcharts issues and find reason why it's not updating
      //  update model, update function and updateOrCreateChart not updating the chart
      setTimeout(() => {
        // hack to update highcharts
        // when new data was added
        window.dispatchEvent(new Event('resize'));
      });
    }
  }

  Highcharts: typeof Highcharts = Highcharts;
  chartOptions: Highcharts.Options = {
    chart: {
      type: 'column',
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Price'
      }
    },
    title: {
      text: 'CSV bar chart',
    },
    series: []
  };


  /**
   * Build chart based on CSV data
   * @param result - CSV data array
   */
  private static buildBarsChart(result: Array<any>): BuildBarsChartResponse {
    const [header, ...series] = result;
    const [chartTitle, yAxisTitle] = header;

    return {
      yAxisTitle,
      chartTitle,
      series: series.map(row => {
        const [name, ...data] = row;
        return {
          name,
          // highchart series data should be number otherwise it will show nothing
          data: data.map(item => parseFloat(item))
        };
      }) as Array<SeriesOptionsType>
    };
  }


  constructor() {
  }


  ngOnInit(): void {
  }
}
