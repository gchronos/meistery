import {NgxsDispatchPluginModule} from '@ngxs-labs/dispatch-decorator';
import {NgxsStoragePluginModule} from '@ngxs/storage-plugin';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {NgxsModule} from '@ngxs/store';

import {AppStateList, AppStateStores} from '@app/core/store';


@NgModule({
  imports: [
    CommonModule,
    NgxsModule.forRoot([...AppStateStores]),
    NgxsStoragePluginModule.forRoot({
      key: [AppStateList.csvForm] // add objects that need to store locally
    }),
    NgxsDispatchPluginModule.forRoot(),
  ],
  exports: [
    NgxsModule,
    NgxsStoragePluginModule,
    NgxsDispatchPluginModule,
  ]
})
export class StoreModule {
}
