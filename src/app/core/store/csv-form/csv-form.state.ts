import {Action, State, StateContext} from '@ngxs/store';

import {CsvFormStateInterface} from '@app/core/store/csv-form/csv-form.model';
import {SaveCSVForm} from '@app/core/store/csv-form/csv-form.actions';


@State<CsvFormStateInterface>({
  name: 'csvForm',
  defaults: {
    form: null
  }
})
export class CsvFormState {

  @Action(SaveCSVForm)
  saveCSVForm({patchState}: StateContext<CsvFormStateInterface>, {payload}: SaveCSVForm) {
    patchState({
      form: {...payload}
    });
  }
}
