import {Dispatch} from '@ngxs-labs/dispatch-decorator';
import {Injectable} from '@angular/core';
import {Select} from '@ngxs/store';
import {Observable} from 'rxjs';

import {SaveCSVForm} from '@app/core/store/csv-form/csv-form.actions';
import {CsvFormModel} from '@app/core/store/csv-form/csv-form.model';
import {AppState} from '@app/core/store';



@Injectable({providedIn: 'root'})
export class CsvFormService {

    @Select((state: AppState) => state.csvForm.form)
    public form$: Observable<CsvFormModel>;

    @Dispatch()
    public saveCSVForm(payload: CsvFormModel) {
        return new SaveCSVForm(payload);
    }
}
