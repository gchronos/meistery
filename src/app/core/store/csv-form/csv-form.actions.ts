import {CsvFormModel} from '@app/core/store/csv-form/csv-form.model';

export class SaveCSVForm {
  static readonly type = '[SaveCSVForm]: action';

  constructor(public payload: CsvFormModel) {
  }
}





