export interface CsvFormStateInterface {
  form: CsvFormModel;
}

export interface CsvFormModel {
  name: string;
  age: string;
  email: string;
  country: {formatted_address: string};
  city: {formatted_address: string};
  uploadedCSVFile: File;
  uploadedCSVFileName: string;
  date: Date;
  textCSV: string;
}

