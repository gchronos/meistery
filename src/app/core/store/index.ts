import {CsvFormStateInterface} from '@app/core/store/csv-form/csv-form.model';
import {CsvFormState} from '@app/core/store/csv-form/csv-form.state';

export interface AppState {
  csvForm: CsvFormStateInterface;
}

export enum AppStateList {
  csvForm = 'csvForm',
}

export const AppStateStores = [
  CsvFormState,
];
